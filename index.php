<?php
    require_once __DIR__ . '/vendor/autoload.php';
    \Sentry\init(['dsn' => 'https://glet_2495403ec12e7da63b5d4c1e22174972@observe.gitlab.com:443/errortracking/api/v1/projects/51513525' ]);

    try {
        $this->functionFailsForSure();
    } catch (\Throwable $exception) {
        \Sentry\captureException($exception);
    }
